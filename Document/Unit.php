<?php
namespace Webit\Common\UnitBundle\Document;
use Webit\Common\UnitBundle\Model\UnitInterface;
use Doctrine\ODM\PHPCR\Mapping\Annotations as PHPCRODM;
use Webit\Bundle\PHPCRToolsBundle\Document\Generic;

/**
 * Webit\Common\UnitBundle\Document\Unit\Unit
 * @author dbojdo
 */
class Unit extends Generic implements UnitInterface {
	/**
	 * @var string
	 */
	protected $measure;

	/**
	 * @var string
	 */
	protected $symbol;

	/**
	 * 
	 * @var string
	 */
	protected $label;

	/**
	 * @var string
	 */
	protected $code;
	
	/**
	 * @return string
	 */
	public function getMeasure() {
		return $this->measure;
	}

	public function setMeasure($measure) {
		if(!empty($this->measure) && $this->measure != $measure) {
			throw new \Exception('Measure can not be modified');
		}
		
		$this->measure = $measure;
	}
	
	/**
	 * @return string
	 */
	public function getSymbol() {
		return $this->symbol;
	}

	/**
	 * 
	 * @param string $symbol
	 */
	public function setSymbol($symbol) {
		$this->symbol = $symbol;
	}

	/**
	 * @return string
	 */
	public function getLabel() {
		return $this->label;
	}

	/**
	 * 
	 * @param string $label
	 */
	public function setLabel($label) {
		$this->label = $label;
	}

		/**
	 * @return string
	 */
	public function getCode() {
		if(empty($this->code)) {
			$this->updateCode();
		}
		
		return $this->code;
	}
	
	private function updateCode() {
		// tylko jeśli nie był ustawiony wcześniej
		if(empty($this->code)) {
			$this->code = $this->measure . ':' . $this->symbol;
		}
	}
	
	public function __toString() {
		return $this->getCode();
	}
	
	public function __sleep() {
		return array('id', 'measure', 'label', 'symbol','code');
	}
}
?>
