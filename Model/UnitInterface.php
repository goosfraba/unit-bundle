<?php
namespace Webit\Common\UnitBundle\Model;
use Webit\Common\DictionaryBundle\Model\DictionaryItem\DictionaryItemInterface;

interface UnitInterface extends DictionaryItemInterface {
	const MEASURE_QUANTITY = 'quantity';
	const MEASURE_LENGTH = 'length';
	const MEASURE_AREA = 'area';
	const MEASURE_VOLUME = 'volume';
	const MEASURE_MASS = 'mass';
	const MEASURE_OTHER = 'other';
	
	/**
	 * @return string
	 */
	public function getMeasure();
	
	/**
	 * @param string $measure
	 */
	public function setMeasure($measure);
	
	/**
	 * @return string
	 */
	public function getSymbol();
	
	/**
	 * @param string $symbol
	 */
	public function setSymbol($symbol);
}
?>
