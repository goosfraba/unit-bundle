<?php
namespace Webit\Common\UnitBundle\Model;

interface UnitAwareInterface {
	/**
	 * @return UnitInterface|null
	 */
	public function getUnit();
	
	/**
	 * @param UnitInterface $unit
	 */
	public function setUnit(UnitInterface $unit = null);
}
?>
