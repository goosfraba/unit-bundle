<?php
namespace Webit\Common\UnitBundle\Entity;

use Webit\Common\UnitBundle\Model\UnitInterface;
use Doctrine\ODM\PHPCR\Mapping\Annotations as PHPCRODM;
use Webit\Bundle\PHPCRToolsBundle\Document\Generic;

/**
 * Webit\Accounting\CommonBundle\Entity\Unit\Unit
 * @author dbojdo
 */
class Unit implements UnitInterface {
	/**
	 * 
	 * @var int
	 */
	protected $id;
	
	/**
	 * @var string
	 */
	protected $measure;
	
	/**
	 * @var string
	 */
	protected $code;
	
	/**
	 * @var string
	 */
	protected $symbol;

	/**
	 * 
	 * @var string
	 */
	protected $label;

	/**
	 * @param string $measure
	 */
	public function __construct($measure) {
		$this->measure = $measure;
	}
	
	/**
	 * @return string
	 */
	public function getMeasure() {
		return $this->measure;
	}
	
	/**
	 * @param string $measure
	 */
	public function setMeasure($measure) {
		if(!empty($this->measure) && $this->measure != $measure) {
			throw new \Exception('Measure can not be modified');
		}
	
		$this->measure = $measure;
	}
	
	/**
	 * @return string
	 */
	public function getSymbol() {
		return $this->symbol;
	}

	/**
	 * 
	 * @param string $symbol
	 */
	public function setSymbol($symbol) {
		$this->symbol = $symbol;
	}

	/**
	 * @return string
	 */
	public function getLabel() {
		return $this->label;
	}

	/**
	 * 
	 * @param string $label
	 */
	public function setLabel($label) {
		$this->label = $label;
	}
	
	/**
	 * @return string
	 */
	public function getCode() {
		if(empty($this->code)) {
			$this->updateCode();
		}
		
		return $this->code;
	}
	
	private function updateCode() {
		// tylko jeśli nie był ustawiony wcześniej
		if(empty($this->code)) {
			$this->code = $this->measure . ':' . $this->symbol;
		}
	}
	
	public function __toString() {
		return $this->getCode();
	}
}
?>
