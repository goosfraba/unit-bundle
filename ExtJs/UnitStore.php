<?php
namespace Webit\Common\UnitBundle\ExtJs;

use Webit\Common\DictionaryBundle\Model\Dictionary\DictionaryInterface;
use Webit\Tools\Object\ObjectUpdater;
use Webit\Bundle\ExtJsBundle\Store\ExtJsStoreAbstract;
use Webit\Bundle\ExtJsBundle\Store\ExtJsJson;
use Webit\Bundle\ExtJsBundle\Store\Sorter\SorterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollectionInterface;

class UnitStore extends ExtJsStoreAbstract {
	/**
   * @var DictionaryInterface
	 */
	protected $ud;
	
	protected $options;
	
	public function __construct(DictionaryInterface $ud, $options = array()) {
		parent::__construct($options);
		$this->ud = $ud;
	}
	
	/**
	 *
	 * @param array $queryParams
	 * @param array $filters
	 * @param stdClass $sort
	 * @param int $page
	 * @param int $limit
	 * @param int $offset
	 * @return ExtJsJson
	 */
	public function getModelList($queryParams, FilterCollectionInterface $filters, SorterCollectionInterface $sorters, $page = 1, $limit = 25, $offset = 0) {
		$collUnits = $this->ud->getItems();
		
		$json = new ExtJsJson();
		$json->setData(array_values($collUnits->toArray()));
		$json->setSerializerGroups(array('generic','Default'));
		
		return $json;
	}
	
	/**
	 *
	 * @param string $id
	 * @return Product
	 */
	public function loadModel($id, $queryParams) {	
		throw new \Exception('Not implemented');
	}
	
	public function createModels(\Traversable $arModelList) {
		$updater = new ObjectUpdater();
		foreach($arModelList as $key=>$unit) {
			$arModelList[$key] = $this->ud->updateItem($unit);
		}
		
		$this->ud->commitChanges();
		
		$json = new ExtJsJson();
		$json->setData($arModelList);
		$json->setSerializerGroups(array('generic','Default'));
		
		return $json;
	}
	
	public function updateModels(\Traversable $arModelList) {
		$updater = new ObjectUpdater();
		foreach($arModelList as $key=>$sUnit) {
			$unit = $this->ud->getItem($sUnit->getCode());
			$updater->fromObject($sUnit,$unit,array('code','measure'));
			
			$arModelList[$key] = $this->ud->updateItem($unit);
		}
		
		$this->ud->commitChanges();
		
		$json = new ExtJsJson();
		$json->setData($arModelList);
		$json->setSerializerGroups(array('generic','Default'));
		
		return $json;
	}
	
	/**
	 *
	 * @param string $id
	 */
	public function deleteModel($id) {		
		$response = new ExtJsJson();
		$response->setData(true);
		foreach($id as $unit) {
			$this->ud->removeItem($unit);
		}
		$this->ud->commitChanges();
	
		return $response;
	}
	
	public function getDataClass() {
		return $this->ud->getItemClass();
	}
}
?>