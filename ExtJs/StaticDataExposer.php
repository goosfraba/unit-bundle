<?php 
namespace Webit\Common\UnitBundle\ExtJs;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Webit\Bundle\ExtJsBundle\StaticData\StaticDataExposerInterface;

final class StaticDataExposer implements StaticDataExposerInterface, ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

	/**
	 * @return array<key, data>
	 */
	public function getExposedData() {
		$staticData = array();
		
		$staticData['unit'] = $this->container->get('webit_common_unit.unit_dictionary')->getItems()->getValues();
		
		return $staticData;
	}

	public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
