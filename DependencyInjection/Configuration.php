<?php

namespace Webit\Common\UnitBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('webit_common_unit');

        $rootNode
        ->children()
        	->scalarNode('use_extjs')->defaultTrue()->end()
        	->scalarNode('storage_type')->defaultValue('orm')->end()
        	->scalarNode('dictionary_name')->defaultValue('unit')->end()
        	->arrayNode('orm')->addDefaultsIfNotSet()
	        	->children()
	        		->scalarNode('item_class')->defaultValue('Webit\Common\UnitBundle\Entity\Unit')->end()
	        	->end()
        	->end()
        	->arrayNode('phpcr')->addDefaultsIfNotSet()
        		->children()
        			->scalarNode('item_class')->defaultValue('Webit\Common\UnitBundle\Document\Unit')->end()
        			->scalarNode('root')->defaultValue('/units')->end()
        		->end()
        	->end()
        ->end();

        return $treeBuilder;
    }
}
