<?php
namespace Webit\Common\UnitBundle\Command;

use Webit\Common\UnitBundle\Model\UnitManagerInterface;

use Symfony\Component\Yaml\Yaml;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class LoadUnitsCommand extends ContainerAwareCommand {
	private $inputFile;
	
	/**
	 * 
	 * @var UnitManagerInterface
	 */
	private $unitManager;
	
	protected function configure() {
		parent::configure();
		$this->setName('webit:units:load')
		->addArgument('file', InputArgument::OPTIONAL)
		->setDescription('Load units from file');
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Console\Command.Command::initialize()
	 */
	protected function initialize(InputInterface $input, OutputInterface $output) {
		$this->unitManager = $this->getContainer()->get('webit_common_unit.unit_dictionary');

		$file = $input->getArgument('file');
		$file = $file ?: $this->getDefaultUnitsFile();
		
		$this->inputFile = $file;
	}
	
	private function getDefaultUnitsFile() {
		return __DIR__ .'/../Resources/static/units.yml';
	}
	
	protected function execute(InputInterface $input, OutputInterface $output) {
		if(!is_file($this->inputFile)) {
			$output->writeln('<error>Input file "'.$this->inputFile.'" not found.</error>');
			return false;
		}
		
		$arUnits = Yaml::parse($this->inputFile);	
		if(!is_array($arUnits)) {
			$output->writeln('<error>Cannot parse input file.</error>');
		}
		
		foreach($arUnits as $measure=>$arUnits) {
			foreach($arUnits as $arUnit) {
				$code = $measure . ':' . $arUnit['symbol'];
				$unit = $this->unitManager->getItem($code) ?: $this->unitManager->createItem();
				$unit->setMeasure($measure);
				$unit->setSymbol($arUnit['symbol']);
				$unit->setLabel($arUnit['label']);
				
				$this->unitManager->updateItem($unit);
			}
		}
		
		$this->unitManager->commitChanges();
	}
}
?>
