<?php
namespace Webit\Common\UnitBundle\Annotation;

use Doctrine\Common\Annotations\Annotation as DoctrineAnnotation;

/**
 * 
 * @author dbojdo
 * @Annotation
 */
final class PersistenceField extends DoctrineAnnotation {
	/** @var string */
	public $for;
}
?>
